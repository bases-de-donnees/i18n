create database `i18n`;
use `i18n`;

-- pays

create table pays (
  `code-2` varchar(2) not null primary key comment 'iso 3166-1 alpha 2',
  `code-3` varchar(3) not null comment 'iso 3166-1 alpha 3',
  `code-num` integer not null comment 'iso 3166-1 numeric',
  `nom` varchar(45) not null comment 'nom du pays en français',
  `drapeau-unicode` char(2),
  `drapeau-svg` text
) engine=innodb;

load data infile '/tmp/pays.csv'
replace into table i18n.pays
fields terminated by ',' enclosed by '\"'
lines terminated by '\n'
ignore 1 lines;

-- langue

create table langue
(
  `code` char(3) not null primary key,
  `langue` varchar(20),
  `français` varchar(20),
  `direction` bit
) engine=innodb;

load data infile '/tmp/langue.csv'
replace into table i18n.langue
fields terminated by ','
lines terminated by '\r\n'
ignore 1 lines;
