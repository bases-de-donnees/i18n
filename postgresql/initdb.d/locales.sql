CREATE TABLE IF NOT EXISTS locales
(
  code character varying(6) NOT NULL,
  nom character varying(46),
  local character varying(46),
  CONSTRAINT locales_pkey PRIMARY KEY (code)
);
